<?php

namespace Boost\XML;

boost()->add_callable('xml', 'Boost\XML\XMLParser');

class XMLParser extends \Boost\Library {

	private $parser;
	private $root = null;
	private $depth = 0;
	private $prev_depth = 0;
	private $current_node = null;
	
	function __constructor()
	{
		echo 'Constructor Ran for XML Lib';
	}

	function parse($data)
	{
		$this->root = new XMLNode('root');

		$this->current_node = $this->root;

		if (!(is_resource(($this->parser = xml_parser_create_ns()))))
			throw new Exception('There was an error creating the XML Parser');

		if (!(xml_set_object($this->parser, $this)))
			throw new Exception('There was an error setting the xml parsers object');

		if (xml_set_default_handler($this->parser, 'parse_default_handler') === false)
			throw new Exception('There was an error initializing the default parsing handler');

		if (xml_set_element_handler($this->parser, 'parse_start_element_handler', 'parse_end_element_handler') === false)
			throw new Exception('There was an error initializing the starting/ending element parsing handler.');

		if (xml_set_character_data_handler($this->parser, 'parse_character_element_handler') === false)
			throw new Exception('There was an error initializing the character data parsing handler.');

		if (xml_parse($this->parser, $data, true) === 0)
			throw new Exception('XML ERROR ['.xml_get_current_line_number($this->parser).']: '.xml_error_string(xml_get_error_code($this->parser)));

		xml_parser_free($this->parser);

		return $this->root;
	}

	private function parse_start_element_handler($parser, $name, $attribs)
	{

		$parts = explode(':', $name);

		if (count($parts) > 2)
		{
			$name = substr($name, strrpos($name, ':') + 1);
			$this->root->addNSChild($name, $attribs);
		}
		else
		{
			$new_node = new XMLNode(strtolower($name), $attribs);

			$new_node->setParent($this->current_node);

			$this->current_node = &$this->current_node->addChild($new_node);
		}
	}

	private function parse_end_element_handler($parser, $name)
	{
		$this->depth--;

		$this->depth = ($this->depth < 0) ? 0 : $this->depth;

		if ($this->current_node->parent() !== null)
			$this->current_node = &$this->current_node->parent();
		else
			$this->current_node = &$this->root;

	}

	private function parse_character_element_handler($parser, $data)
	{
		$this->current_node->setCData($data);
	}

}
