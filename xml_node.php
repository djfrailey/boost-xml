<?php

namespace Boost\XML;

class XMLNode
{

	private $children = array();
	
	private $next_sibling = null;
	private $prev_sibling = null;
	private $parent = null;
	private $namespace = array();

	private $attributes = array();
	private $name = null;
	private $c_data = null;

	function __construct($name = null, $attributes = array())
	{
		$this->name = $name;
		$this->attributes = $attributes;
	}

	function getName()
	{
		return $this->name;
	}

	function getCData()
	{
		return $this->c_data;
	}

	function getAttributes()
	{
		return $this->attributes;
	}

	function getNS($namespace)
	{
		if ($this->hasNS($namespace))
		{
			return $this->namespace[$namespace];
		}

		return false;
	}

	function setNextSibling(XMLNode &$node)
	{
		$this->next_sibling = &$node;
	}

	function setPrevSibling(XMLNode &$node)
	{
		$this->prev_sibling = &$node;
	}

	function hasAttr($attr)
	{
		return array_key_exists($this->attributes);
	}

	function hasChildren()
	{
		return (count($this->children) > 0 ? true : false);
	}

	function addNS($namespace)
	{
		$this->namespace[$namespace] = array();
	}

	function addNSChild($namespace, $node)
	{
		if ($this->hasNS($namespace))
		{
			$this->namespace[$namespace][] = $node;
		}
		else
		{
			$this->addNS($namespace);
			$this->addNSChild($namespace, $node);
		}
	}

	function hasNS($namespace)
	{
		return array_key_exists($namespace, $this->namespace);
	}

	function &addChild(XMLNode &$node)
	{
		if ($this->hasChildren()) {
			$sibling = &$this->children[ count($this->children) - 1 ];
			$this->setPrevSibling($sibling);
			$sibling->setNextSibling($node);
		}

		array_push($this->children, $node);
		return $node;
	}

	function setParent(&$parent)
	{
		$this->parent = &$parent;
	}

	function setCData($data)
	{
		$this->c_data = $data;
	}

	function &parent()
	{
		return $this->parent;
	}

	function &next()
	{
		return $this->next_sibling;
	}

	function &prev()
	{
		return $this->prev_sibling;
	}

}